#!/bin/sh

BB_HOME=/opt/hogger
JAVA_HOME=/usr/lib/jvm/java-8-oracle

ACCESS_TOKEN="1007815746-VL4USjXfy9TQG1cGXPnZ8AaZG2KkUxnm7FsaoWx"
ACCESS_TOKEN_SECRET="bQagDthovJheSDIv5tA5sn9otLIn4Cngwb6YibvnLI"
CONSUMER_KEY="RwQGOkuT7ti89bKDIQ9hhg"
CONSUMER_SECRET="HQNgFIbdOLrTLC2tTKTK2nhLEovmrij9GM5x3VS0V0"

TWITTER_OPTS="-Doauth.consumerKey=${CONSUMER_KEY} -Doauth.consumerSecret=${CONSUMER_SECRET} -Doauth.accessToken=${ACCESS_TOKEN} -Doauth.accessTokenSecret=${ACCESS_TOKEN_SECRET}"

upgrade ()
{
  cd $BB_HOME/baconbot && git pull && mvn clean package -Dmaven.test.skip
  if [ -f $BB_HOME/baconbot/target/baconbot.zip ]; then
    cp $BB_HOME/compiled $BB_HOME/prev-compiled
    cd $BB_HOME/baconbot && (git rev-parse HEAD > $BB_HOME/compiled)
    cd $BB_HOME/baconbot && (git shortlog -sne > $BB_HOME/shortlog)
    cd $BB_HOME/baconbot && (git log --pretty=format:'(%h) %cN: %s' `cat $BB_HOME/prev-compiled`..`cat $BB_HOME/compiled` > $BB_HOME/changes)
    cd $BB_HOME && unzip -o $BB_HOME/baconbot/target/baconbot.zip -d $BB_HOME/dist
    #if [ ! -L $BB_HOME/baconbot/fishnet.properties ]; then
    # cd $BB_HOME/baconbot && ln -s ../fishnet.properties
    #fi
  fi
}

run ()
{
  cd $BB_HOME/dist/baconbot && java -jar -Dshortlog=$BB_HOME/shortlog -Dchanges=$BB_HOME/changes -Dwork.dir=$BB_HOME $TWITTER_OPTS -Dmongodb.host=mongodb baconbot.jar /opt/properties/fishnet.properties
}

touch $BB_HOME/upgrade
while [ -f $BB_HOME/upgrade ]; do
  upgrade
  rm $BB_HOME/upgrade
  run
done

