from ubuntu

RUN apt-get update
RUN apt-get install -y git
RUN mkdir /opt/hogger
WORKDIR /opt/hogger
RUN git clone https://mjensen@bitbucket.org/mjensen/baconbot.git
#WORKDIR /opt/baconbot

RUN apt-get install -y curl ruby
RUN curl -s https://bitbucket.org/mjensen/mvnvm/raw/master/mvn > /usr/bin/mvn && chmod 0755 /usr/bin/mvn
#ENV PATH="~/bin:${PATH}"

RUN apt-get install -y software-properties-common && add-apt-repository ppa:linuxuprising/java && apt-get update
RUN apt-get install sudo
RUN echo oracle-java12-installer shared/accepted-oracle-license-v1-2 select true | sudo /usr/bin/debconf-set-selections
RUN apt-get install -y oracle-java12-installer
RUN apt-get install -y oracle-java12-set-default

WORKDIR /opt/hogger/baconbot
RUN mvn clean install -DskipTests
COPY run.sh /opt/hogger

CMD [ "/opt/hogger/run.sh" ]
